package ar.fiuba.tdd.tp1.tests.parser;

import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.parsers.IParser;
import ar.fiuba.tdd.tp1.parsers.StringParser;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class ParserStringTest {
    @Test
    public void parseSingleStringTest() {
        IParser parser = StringParser.getInstance();
        IEvaluable string = parser.parse("$3e3");
        assertTrue("$3e3".equals(string.getValue()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseSingleStringNoDoubleTest() {
        IParser parser = StringParser.getInstance();
        IEvaluable string = parser.parse("33");
        string.asDouble();
    }
}