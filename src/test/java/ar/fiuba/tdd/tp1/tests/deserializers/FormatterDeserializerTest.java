package ar.fiuba.tdd.tp1.tests.deserializers;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.deserializers.FormatterDeserializer;
import ar.fiuba.tdd.tp1.formatters.DateFormatter;
import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;
import ar.fiuba.tdd.tp1.formatters.MoneyFormatter;
import ar.fiuba.tdd.tp1.formatters.NumberFormatter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FormatterDeserializerTest {

    private Gson gson;

    @Before
    public void setUp() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DoubleFormatter.class, new FormatterDeserializer());
        gson = gsonBuilder.serializeNulls().create();
    }

    @Test
    public void numberFormatterDeserializationTest() {
        String json = "{\"Number." + NumberFormatter.DECIMAL + "\":\"3\"}";
        DoubleFormatter formatter = gson.fromJson(json, DoubleFormatter.class);
        assertTrue(formatter.formatDouble(3).equals("3,000"));
    }

    @Test
    public void dateFormatterDeserializationTest() {
        String json = "{\"Date." + DateFormatter.FORMAT + "\":\"" + DateFormatter.DMY_FORMAT + "\"}";
        DoubleFormatter formatter = gson.fromJson(json, DoubleFormatter.class);
        assertTrue(formatter.formatDouble(86400).equals("02-01-1970"));
    }

    @Test
    public void moneyFormatterWithDecimalsDeserializationTest() {
        String json = "{"
                + "\"Money." + MoneyFormatter.SYMBOL + "\":\"$\","
                + "\"Money." + MoneyFormatter.DECIMAL + "\":\"2\""
                + "}";
        DoubleFormatter formatter = gson.fromJson(json, DoubleFormatter.class);
        assertTrue(formatter.formatDouble(5).equals("$ 5.00"));
    }

    @Test
    public void moneyFormatterNoDecimalsDeserializationTest() {
        String json = "{"
                + "\"Money." + MoneyFormatter.SYMBOL + "\":\"£\","
                + "\"Money." + MoneyFormatter.DECIMAL + "\":\"0\""
                + "}";
        DoubleFormatter formatter = gson.fromJson(json, DoubleFormatter.class);
        assertTrue(formatter.formatDouble(8).equals("£ 8"));
    }
}
