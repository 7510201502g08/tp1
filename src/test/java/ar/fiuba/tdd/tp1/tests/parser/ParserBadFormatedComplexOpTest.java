package ar.fiuba.tdd.tp1.tests.parser;

import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.Parser;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ParserBadFormatedComplexOpTest {

    @Test
    public void parseAdd1Test() {
        IEvaluable formula = Parser.getInstance().parse("=   2 + ( 1    + 4 )");
        assertTrue(formula.asDouble() == 7);
    }

    @Test
    public void parseAdd2Test() {
        IEvaluable formula = Parser.getInstance().parse("= (2 + 1) + 4 ");
        assertTrue(formula.asDouble() == 7);
    }

    @Test
    public void parseSubtract1Test() {
        IEvaluable formula = Parser.getInstance().parse("= 3 - (2 - 1)");
        assertTrue(formula.asDouble() == 2);
    }

    @Test
    public void parseSubtract2Test() {
        IEvaluable formula = Parser.getInstance().parse("= (   3 - 2) - 1");
        assertTrue(formula.asDouble() == 0);
    }

    @Test
    public void parseCombined1Test() {
        IEvaluable formula = Parser.getInstance().parse("= 2 + (1 - 4 )");
        assertTrue(formula.asDouble() == -1);
    }

    @Test
    public void parseCombined2Test() {
        IEvaluable formula = Parser.getInstance().parse("= 2 - (1 + 4)");
        assertTrue(formula.asDouble() == -3);
    }

    @Test
    public void parseCombined3Test() {
        IEvaluable formula = Parser.getInstance().parse("= ( 2 + 1) - 4");
        assertTrue(formula.asDouble() == -1);
    }

    @Test
    public void parseCombined4Test() {
        IEvaluable formula = Parser.getInstance().parse("= (2 - 1) + 4   ");
        assertTrue(formula.asDouble() == 5);
    }

    @Test
    public void parseMultiple1Test() {
        IEvaluable formula = Parser.getInstance().parse("= (2 + 2) + (1 + 4)");
        assertTrue(formula.asDouble() == 9);
    }

    @Test
    public void parseMultiple2Test() {
        IEvaluable formula = Parser.getInstance().parse("= (2 + 2)      -     (    1 + 4)");
        assertTrue(formula.asDouble() == -1);
    }

    @Test
    public void parseMultiple3Test() {
        IEvaluable formula = Parser.getInstance().parse("= (2 + 2) - (1 - 4)");
        assertTrue(formula.asDouble() == 7);
    }

    @Test
    public void parseMultiple4Test() {
        IEvaluable formula = Parser.getInstance().parse("= (2 - 2) - (1 + 4)");
        assertTrue(formula.asDouble() == -5);
    }

    @Test
    public void parseMultiple5Test() {
        IEvaluable formula = Parser.getInstance().parse("= (2 - 2) - (1 -     4 )");
        assertTrue(formula.asDouble() == 3);
    }

    @Test
    public void parseDeepRecursive1Test() {
        IEvaluable formula = Parser.getInstance().parse("= ((2 - 2) - (1 - 4)) + 2");
        assertTrue(formula.asDouble() == 5);
    }

    @Test
    public void parseDeepRecursive2Test() {
        IEvaluable formula = Parser.getInstance().parse("=    (((2 - 2) + 4) -      7     ) + 2   ");
        assertTrue(formula.asDouble() == -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void missingBraces1Test() {
        Parser.getInstance().parse("= ((2 - 2) + 2");
    }

    @Test(expected = IllegalArgumentException.class)
    public void missingBraces2Test() {
        Parser.getInstance().parse("= ( ( 2 ) - 2 ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void missingBraces3Test() {
        Parser.getInstance().parse("= ( 2");
    }
}
