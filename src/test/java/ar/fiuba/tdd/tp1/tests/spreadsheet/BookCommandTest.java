package ar.fiuba.tdd.tp1.tests.spreadsheet;

import ar.fiuba.tdd.tp1.Book;
import org.junit.Before;
import org.junit.Test;


public class BookCommandTest {

    private Book ssheet;

    @Before
    public void setUp() {
        ssheet = new Book();
    }

    @Test(expected = IllegalArgumentException.class)
    public void evaluateEmptyCommandTest() {
        ssheet.evaluateCommand("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void evaluateNullCommandTest() {
        ssheet.evaluateCommand(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void evaluateWrongCommandTest() {
        ssheet.evaluateCommand("FLY");
    }
}
