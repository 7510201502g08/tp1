package ar.fiuba.tdd.tp1.tests.parser;

import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.Parser;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ParserSingleOpTest {

    @Test
    public void parseSingleValueTest() {
        IEvaluable formula = Parser.getInstance().parse("2");
        assertTrue(formula.asDouble() == 2);
    }

    @Test
    public void parseAdd1Test() {
        IEvaluable formula = Parser.getInstance().parse("= 2 + 3");
        assertTrue(formula.asDouble() == 5);
    }

    @Test
    public void parseAdd2Test() {
        IEvaluable formula = Parser.getInstance().parse("= 3 + 2");
        assertTrue(formula.asDouble() == 5);
    }

    @Test
    public void parseSubtract1Test() {
        IEvaluable formula = Parser.getInstance().parse("= 3 - 2");
        assertTrue(formula.asDouble() == 1);
    }

    @Test
    public void parseSubtract2Test() {
        IEvaluable formula = Parser.getInstance().parse("= 2 - 3");
        assertTrue(formula.asDouble() == -1);
    }

    @Test
    public void parseAddNegative1Test() {
        IEvaluable formula = Parser.getInstance().parse("= 3 + -2");
        assertTrue(formula.asDouble() == 1);
    }

    @Test
    public void parseAddNegative2Test() {
        IEvaluable formula = Parser.getInstance().parse("= -3 + 2");
        assertTrue(formula.asDouble() == -1);
    }

    @Test
    public void parseAddNegative3Test() {
        IEvaluable formula = Parser.getInstance().parse("= -3 + -2");
        assertTrue(formula.asDouble() == -5);
    }

    @Test
    public void parseSubtractNegative1Test() {
        IEvaluable formula = Parser.getInstance().parse("= 3 - -2");
        assertTrue(formula.asDouble() == 5);
    }

    @Test
    public void parseSubtractNegative2Test() {
        IEvaluable formula = Parser.getInstance().parse("= -3 - 2");
        assertTrue(formula.asDouble() == -5);
    }

    @Test
    public void parseSubtractNegative3Test() {
        IEvaluable formula = Parser.getInstance().parse("= -3 - -2");
        assertTrue(formula.asDouble() == -1);
    }
}
