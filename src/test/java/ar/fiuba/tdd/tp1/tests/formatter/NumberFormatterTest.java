package ar.fiuba.tdd.tp1.tests.formatter;

import ar.fiuba.tdd.tp1.formatters.FormatterFactory;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class NumberFormatterTest {

    @Test
    public void formatDefaultDecimalTest() {
        String result = FormatterFactory.getNumberFormatter().formatDouble(3);
        assertTrue(result.equals("3,00"));
    }

    @Test
    public void formatNoDecimalTest() {
        String result = FormatterFactory.getNumberFormatter(0).formatDouble(3);
        assertTrue(result.equals("3"));
    }

    @Test
    public void formatFiveDecimalTest() {
        String result = FormatterFactory.getNumberFormatter(5).formatDouble(37);
        assertTrue(result.equals("37,00000"));
    }


    @Test
    public void formatDefaultDoubleTest() {
        String result = FormatterFactory.getNumberFormatter().formatDouble(3.0);
        assertTrue(result.equals("3,00"));
    }

    @Test
    public void formatNoDoubleTest() {
        String result = FormatterFactory.getNumberFormatter(0).formatDouble(3.00);
        assertTrue(result.equals("3"));
    }

    @Test
    public void formatFiveDoubleTest() {
        String result = FormatterFactory.getNumberFormatter(5).formatDouble(37.00);
        assertTrue(result.equals("37,00000"));
    }

    @Test
    public void formatStringFourDoubleTest() {
        String result = FormatterFactory.getNumberFormatter("4").formatDouble(11.0);
        assertTrue(result.equals("11,0000"));
    }
}
