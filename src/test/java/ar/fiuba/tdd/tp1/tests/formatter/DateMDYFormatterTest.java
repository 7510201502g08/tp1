package ar.fiuba.tdd.tp1.tests.formatter;


import ar.fiuba.tdd.tp1.formatters.FormatterFactory;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DateMDYFormatterTest {

    @Test
    public void formatEpochTest() {
        String result = FormatterFactory.getDateMDYFormatter().formatDouble(0);
        assertTrue(result.equals("01-01-1970"));
    }

    @Test
    public void formatADayOneDecimalTest() {
        String result = FormatterFactory.getDateMDYFormatter().formatDouble(1445223683);
        assertTrue(result.equals("10-19-2015"));
    }

    @Test
    public void formatADayTwoDecimalTest() {
        String result = FormatterFactory.getDateMDYFormatter().formatDouble(1445223684);
        assertTrue(result.equals("10-19-2015"));
    }

    @Test
    public void formatADayThreeDecimalTest() {
        String result = FormatterFactory.getDateMDYFormatter().formatDouble(1445223685);
        assertTrue(result.equals("10-19-2015"));
    }
}
