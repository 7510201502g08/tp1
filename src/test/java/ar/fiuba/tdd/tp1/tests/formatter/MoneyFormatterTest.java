package ar.fiuba.tdd.tp1.tests.formatter;

import ar.fiuba.tdd.tp1.formatters.FormatterFactory;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MoneyFormatterTest {

    @Test
    public void formatDefaultDecimalTest() {
        String result = FormatterFactory.getMoneyFormatter("$").formatDouble(3);
        assertTrue(result.equals("$ 3.00"));
    }

    @Test
    public void formatWithoutDecimalTest() {
        String result = FormatterFactory.getMoneyFormatter("u$s", false).formatDouble(3);
        assertTrue(result.equals("u$s 3"));
    }

    @Test
    public void formatWithDecimalTest() {
        String result = FormatterFactory.getMoneyFormatter("u$s", true).formatDouble(37);
        assertTrue(result.equals("u$s 37.00"));
    }


    @Test
    public void formatDefaultDoubleTest() {
        String result = FormatterFactory.getMoneyFormatter("$").formatDouble(3.0);
        assertTrue(result.equals("$ 3.00"));
    }

    @Test
    public void formatWithoutDoubleTest() {
        String result = FormatterFactory.getMoneyFormatter("$", false).formatDouble(3.00);
        assertTrue(result.equals("$ 3"));
    }

    @Test
    public void formatWithDoubleTest() {
        String result = FormatterFactory.getMoneyFormatter("$", true).formatDouble(37.00);
        assertTrue(result.equals("$ 37.00"));
    }
}
