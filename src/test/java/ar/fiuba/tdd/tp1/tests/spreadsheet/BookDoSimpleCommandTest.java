package ar.fiuba.tdd.tp1.tests.spreadsheet;

import ar.fiuba.tdd.tp1.Book;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class BookDoSimpleCommandTest {

    private Book ssheet;
    private ArrayList<String> parameters;


    @Before
    public void setUp() {
        ssheet = new Book();
        parameters = new ArrayList<>();
        parameters.add("H1");
        ssheet.createSheet(parameters);
        parameters.clear();
    }

    @After
    public void tearDown() {
        ssheet = null;
        parameters = null;
    }

    @Test
    public void assignSingleValueAndGet1Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 3.23".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 3.23);
    }

    @Test
    public void assignSingleValueAndGet2Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 3.23".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 = 633".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 3.23);
    }

    @Test
    public void assignTwoNumberSumAndGetTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 9.5 + 5".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 14.5);
    }

    @Test
    public void assignTwoNumberSubtractAndGetTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A1 = 9.5 - 1.11".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 8.39);
    }

    @Test
    public void assignCellAndNumberSumAndGetTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = !H1.C1 + 4".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C2");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 5);
    }

    @Test
    public void assignTwoCellSumAndGet1Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = 5.5".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C3 = !H1.C1 + !H1.C2".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C3");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 6.5);
    }

    @Test
    public void assignTwoCellSumAndGet2Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 1 + 1".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = !H1.C1 + !H1.C1".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C2");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 4);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getValueOfInvalidCellTest() {
        parameters = new ArrayList<>(Arrays.asList("noCell".split(" ")));
        ssheet.assignValue(parameters);

        Double.parseDouble(ssheet.getValue(parameters));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getValueOfNonInitializedCell1Test() {
        parameters.add("!H1.A1");
        ssheet.getDoubleValue(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getValueOfNonInitializedCell2Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.A2 = -3.5".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H1.C2");
        ssheet.getDoubleValue(parameters);
    }

    @Test(expected = IllegalArgumentException.class)
    public void assignValueToInvalidCellTest() {
        parameters = new ArrayList<>(Arrays.asList("noCell = 5 + 2".split(" ")));
        ssheet.assignValue(parameters);
    }
}
