package ar.fiuba.tdd.tp1.tests.parser;

import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.parsers.MoneyParser;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class ParserMoneyTest {

    @Test
    public void parseSingleValuePrintTest() {
        MoneyParser parser = MoneyParser.getInstance();
        IEvaluable money = parser.parse("$33");
        assertTrue("$ 33.00".equals(money.getValue()));
    }

    @Test
    public void parseSingleValueConvertTest() {
        MoneyParser parser = MoneyParser.getInstance();
        IEvaluable money = parser.parse("$33");
        assertTrue(33 == money.asDouble());
    }

    @Test
    public void parseComplexValuePrintTest() {
        MoneyParser parser = MoneyParser.getInstance();
        IEvaluable money = parser.parse("$33,00");
        assertTrue("$ 33.00".equals(money.getValue()));
    }

    @Test
    public void parseComplexValueConvertTest() {
        MoneyParser parser = MoneyParser.getInstance();
        IEvaluable money = parser.parse("$33,00");
        assertTrue(33 == money.asDouble());
    }

    @Test
    public void parseOverRangeValuePrintTest() {
        MoneyParser parser = MoneyParser.getInstance();
        IEvaluable money = parser.parse("$33,000003");
        assertTrue("$ 33.00".equals(money.getValue()));
    }

    @Test
    public void parseOverRangeValueConvertTest() {
        MoneyParser parser = MoneyParser.getInstance();
        IEvaluable money = parser.parse("$33,000003");
        assertTrue(33.000003 == money.asDouble());
    }
}
