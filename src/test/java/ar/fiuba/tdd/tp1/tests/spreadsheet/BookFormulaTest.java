package ar.fiuba.tdd.tp1.tests.spreadsheet;

import ar.fiuba.tdd.tp1.Book;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class BookFormulaTest {

    private Book ssheet;
    private ArrayList<String> parameters;

    @Before
    public void createSheet() {
        ssheet = new Book();
        parameters = new ArrayList<>();
        parameters.add("H1");
        ssheet.createSheet(parameters);
        parameters.clear();
        parameters.add("H2");
        ssheet.createSheet(parameters);
        parameters.clear();
    }

    @After
    public void tearDown() {
        ssheet = null;
        parameters = null;
    }

    @Test
    public void multipleSheetAssignTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 12".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.A1 = !H1.C1".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H2.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 12);
    }

    @Test
    public void multipleSheetOperation1Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 12".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.C2 = 2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.A1 = !H1.C1 + !H2.C2".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H2.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 14);
    }

    @Test
    public void multipleSheetOperation2Test() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 12".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.C2 = 2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.A1 = !H1.C1 - !H2.C2".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        parameters.add("!H2.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 10);
    }

    @Test
    public void multipleSheetOperationUndoTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 12".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = 2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.A1 = !H1.C1 - !H1.C2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.C2 = 25".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.A1 = !H1.C1 - !H2.C2".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();

        parameters.add("!H2.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == 10);
    }

    @Test
    public void multipleSheetOperationUndoRedoTest() {
        parameters = new ArrayList<>(Arrays.asList("!H1.C1 = 12".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H1.C2 = 2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.A1 = !H1.C1 - !H1.C2".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.C2 = 25".split(" ")));
        ssheet.assignValue(parameters);
        parameters = new ArrayList<>(Arrays.asList("!H2.A1 = !H1.C1 - !H2.C2".split(" ")));
        ssheet.assignValue(parameters);

        parameters.clear();

        ssheet.undo();
        ssheet.redo();

        parameters.add("!H2.A1");
        double result = ssheet.getDoubleValue(parameters);
        assertTrue(result == -13);
    }

}
