package ar.fiuba.tdd.tp1.tests.serializers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.IEvaluable;
import ar.fiuba.tdd.tp1.formatters.*;
import ar.fiuba.tdd.tp1.serializers.CellSerializer;
import ar.fiuba.tdd.tp1.serializers.FormatterSerializer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CellSerializerTest {

    private Gson gson;

    @Before
    public void setUp() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Cell.class, new CellSerializer());
        gsonBuilder.registerTypeAdapter(DoubleFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(NumberFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(MoneyFormatter.class, new FormatterSerializer());
        gsonBuilder.registerTypeAdapter(DateFormatter.class, new FormatterSerializer());
        gson = gsonBuilder.serializeNulls().create();
    }

    @Test
    public void stringCellTest() {
        Cell cell = new Cell("string_cell", () -> "string_value");
        String jsonResult = gson.toJson(cell);
        assertTrue(jsonResult.equals("{\"id\":\"string_cell\",\"value\":\"string_value\",\"type\":\"String\",\"formatter\":null}"));
    }

    @Test
    public void moneyCellTest() {
        Cell cell = new Cell("money_cell", mockMoneyValue());
        cell.setFormatter(FormatterFactory.getMoneyFormatter("$"));
        String jsonResult = gson.toJson(cell);
        assertTrue(jsonResult.equals("{\"id\":\"money_cell\",\"value\":\"$ 8.00\",\"type\":\"Money\",\""
                + "formatter\":{\"Money.symbol\":\"$\",\"Money.decimal\":\"2\"}}"));
    }

    private IEvaluable mockMoneyValue() {
        return new IEvaluable() {

            private DoubleFormatter formatter = FormatterFactory.getMoneyFormatter("$");
            private double value = 8;

            public String getValue() {
                return formatter.formatDouble(value);
            }

            public double asDouble() {
                return value;
            }

            public DoubleFormatter getFormatter() {
                return formatter;
            }
        };
    }
}
