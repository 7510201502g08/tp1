package ar.fiuba.tdd.tp1.tests.deserializers;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.deserializers.CellDeserializer;
import ar.fiuba.tdd.tp1.deserializers.FormatterDeserializer;
import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CellDeserializerTest {

    private Gson gson;

    @Before
    public void setUp() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Cell.class, new CellDeserializer());
        gsonBuilder.registerTypeAdapter(DoubleFormatter.class, new FormatterDeserializer());
        gson = gsonBuilder.serializeNulls().create();
    }

    @Test
    public void stringCellTest() {
        String cellString = "{\"id\":\"string_cell\",\"value\":\"string_value\",\"type\":\"String\",\"formatter\":null}";
        Cell cellResult = gson.fromJson(cellString, Cell.class);
        assertTrue(cellResult.id().equals("string_cell"));
        assertTrue(cellResult.getValue().equals("string_value"));
        assertNull(cellResult.getFormatter());
    }

    @Test
    public void moneyCellTest() {
        String cellString = "{\"id\":\"money_cell\",\"value\":\"$ 8.00\",\"type\":\"Money\",\""
                + "formatter\":{\"Money.symbol\":\"$\",\"Money.decimal\":\"2\"}}";
        Cell cellResult = gson.fromJson(cellString, Cell.class);
        assertTrue(cellResult.id().equals("money_cell"));
        assertTrue(cellResult.getValue().equals("$ 8.00"));
        assertNotNull(cellResult.getFormatter());
        assertTrue(cellResult.getFormatter().getDataType().equals("Money"));
    }
}
