package ar.fiuba.tdd.tp1.deserializers;

import com.google.gson.*;

import ar.fiuba.tdd.tp1.Book;
import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.Sheet;

import java.lang.reflect.Type;

public class BookDeserializer implements JsonDeserializer<Book> {

    @Override
    public Book deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Book book = new Book(jsonObject.get("name").getAsString());
        for (JsonElement element : jsonObject.get("cells").getAsJsonArray()) {
            JsonObject jsonCell = element.getAsJsonObject();
            Sheet sheet = getSheet(book, jsonCell.get("sheet").getAsString());
            Cell cell = context.deserialize(jsonCell, Cell.class);
            sheet.addCell(cell.id(), cell);
        }
        return book;
    }

    private Sheet getSheet(Book book, String sheetId) {
        if (book.getSheetsNames().contains(sheetId)) {
            return book.getSheet(sheetId);
        }
        Sheet newSheet = new Sheet();
        book.getSheetMap().put(sheetId, newSheet);
        return newSheet;
    }
}
