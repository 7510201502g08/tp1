package ar.fiuba.tdd.tp1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Range<T> implements Iterable<T> {

    List<T> evaluables;

    Range() {
        evaluables = new ArrayList<>();
    }

    public void add(T element) {
        evaluables.add(element);
    }

    @Override
    public Iterator<T> iterator() {
        return evaluables.iterator();
    }
}
