package ar.fiuba.tdd.tp1.formatters;

public class StringFormatter extends DoubleFormatter {
    public StringFormatter() {
    }

    @Override
    public String formatDouble(double param) {
        throw new BadFormatException();
    }

    @Override
    public String getErrorMsg() {
        return "Error:BAD_STRING";
    }

}
