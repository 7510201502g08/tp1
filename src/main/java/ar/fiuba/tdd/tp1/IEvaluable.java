package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;

/**
 * Interface to be implemented by anything that will be evaluated to obtain a value.
 */
//TODO: Check name
public interface IEvaluable {

    String getValue();

    default double asDouble() {
        throw new IllegalArgumentException();
    }

    default DoubleFormatter getFormatter() {
        return null;
    }
}
