package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.EvaluablesFactory;
import ar.fiuba.tdd.tp1.IEvaluable;


public class DoubleParser implements IParser {
    private static DoubleParser ourInstance = new DoubleParser();

    private DoubleParser() {
    }

    public static DoubleParser getInstance() {
        return ourInstance;
    }

    @Override
    public IEvaluable parse(String param) {

        double value = Double.parseDouble(param);

        return EvaluablesFactory.getNumber(value);
    }


}
