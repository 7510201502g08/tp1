package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.EvaluablesFactory;
import ar.fiuba.tdd.tp1.IEvaluable;

public class DateParser implements IParser {
    @SuppressWarnings("CPD-START")
    private static DateParser ourInstance = new DateParser();

    private DateParser() {
    }

    public static DateParser getInstance() {
        return ourInstance;
    }

    @SuppressWarnings("CPD-END")
    @Override
    public IEvaluable parse(String param) {

        return EvaluablesFactory.getDate(param);
    }


}
