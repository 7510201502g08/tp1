package ar.fiuba.tdd.tp1.serializers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ar.fiuba.tdd.tp1.Cell;
import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;

import java.lang.reflect.Type;

public class CellSerializer implements JsonSerializer<Cell> {

    public JsonElement serialize(Cell src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonCell = new JsonObject();
        jsonCell.addProperty("id", src.id());
        String cellValue;
        if (src.value() == null) {
            cellValue = null;
        } else {
            cellValue = src.value().getValue();
        }
        jsonCell.addProperty("value", cellValue);
        DoubleFormatter valueFormatter = src.value().getFormatter();
        jsonCell.addProperty("type", valueFormatter != null ? valueFormatter.getDataType() : "String");
        jsonCell.add("formatter", context.serialize(src.getFormatter()));
        return jsonCell;
    }
}
