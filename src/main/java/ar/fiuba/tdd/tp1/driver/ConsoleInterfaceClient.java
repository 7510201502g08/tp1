package ar.fiuba.tdd.tp1.driver;

import java.util.Scanner;

public class ConsoleInterfaceClient {

    static Scanner reader;
    static SpreadSheetInterfaceAdapter spreadsheet;

    static String bookName;
    static String sheetName;
    static String cellId;
    static String value;

    static int command = 9;

    public static void main(String[] args) {
        reader = new Scanner(System.in, "UTF-8");
        spreadsheet = new SpreadSheetInterfaceAdapter();
        System.out.println("Welcome to SpreadSheet, group 8.");
        while (command != 0) {
            showMenu();
            command = Integer.parseInt(reader.nextLine());
            chooseAndExecuteOption();
        }
        System.out.println("Gracias, vuelva prontos.");
    }

    private static void chooseAndExecuteOption() {
        if (command < 5) {
            firstHalf();
        } else {
            secondHalf();
        }
    }

    /*
     * To reduce NCSS complexity
     */
    private static void firstHalf() {
        switch (command) {
            case 1:
                createNewWorkBook();
                break;
            case 2:
                createNewWorkSheet();
                break;
            case 3:
                setCellValue();
                break;
            case 4:
                getCellValueAsDouble();
                break;
            default:
                break;
        }
    }

    /*
     * To reduce NCSS complexity
     */
    private static void secondHalf() {
        switch (command) {
            case 5:
                getCellValueAsString();
                break;
            case 6:
                spreadsheet.undo();
                break;
            case 7:
                spreadsheet.redo();
                break;
            default:
                break;
        }
    }

    private static void createNewWorkSheet() {
        bookName = reader.nextLine();
        sheetName = reader.nextLine();
        spreadsheet.createNewWorkSheetNamed(bookName, sheetName);
    }

    private static void createNewWorkBook() {
        bookName = reader.nextLine();
        spreadsheet.createNewWorkBookNamed(bookName);
    }

    private static void setCellValue() {
        bookName = reader.nextLine();
        sheetName = reader.nextLine();
        cellId = reader.nextLine();
        value = reader.nextLine();
        spreadsheet.setCellValue(bookName, sheetName, cellId, value);
    }

    public static void getCellValueAsDouble() {
        askParameters();
        System.out.println(spreadsheet.getCellValueAsDouble(bookName, sheetName, cellId));
    }

    private static void getCellValueAsString() {
        askParameters();
        System.out.println(spreadsheet.getCellValueAsString(bookName, sheetName, cellId));
    }

    private static void askParameters() {
        bookName = reader.nextLine();
        sheetName = reader.nextLine();
        cellId = reader.nextLine();

    }

    private static void showMenu() {
        System.out.println("Choose the option to execute.\n"
                + "1- Create new Book\n"
                + "2- Create new Sheet\n"
                + "3- Set value to Cell\n"
                + "4- Get value as double from Cell\n"
                + "5- Get value as string from Cell\n"
                + "6- Undo\n"
                + "7- Redo\n"
                + "0- Exit\n");
    }
}
