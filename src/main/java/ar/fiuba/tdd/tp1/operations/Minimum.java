package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IEvaluable;

import java.util.List;

public class Minimum implements IRangeOperation {

    public Minimum() {

    }

    public final String operate(List<IEvaluable> cellList) {

        double min = cellList.get(0).asDouble();
        for (IEvaluable c : cellList) {
            try {
                min = Double.min(min, c.asDouble());
            } catch (IllegalArgumentException e) {
                continue;
            }
        }

        return Double.toString(min);
    }
}
