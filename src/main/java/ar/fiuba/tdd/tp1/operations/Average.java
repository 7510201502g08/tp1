package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IEvaluable;

import java.util.List;

public class Average implements IRangeOperation {

    public Average() {

    }

    public final String operate(List<IEvaluable> cellList) {

        double total = 0;
        int count = 0;
        for (IEvaluable c : cellList) {
            try {
                total += c.asDouble();
                count += 1;
            } catch (Exception e) {
                continue;
            }
        }

        if (count == 0) {
            count = 1;
        }
        return Double.toString(total / count);
    }
}
