package ar.fiuba.tdd.tp1.operations;

/**
 * Represents any binary operation.
 */
public interface IBinaryOperation {
    double operate(double firstOp, double secondOp);

}
