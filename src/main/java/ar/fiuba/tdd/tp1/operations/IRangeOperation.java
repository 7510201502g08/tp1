package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IEvaluable;

import java.util.List;

/**
 * Represents any range operation.
 */
public interface IRangeOperation {
    String operate(List<IEvaluable> cellList);
}
