package ar.fiuba.tdd.tp1.operations;

import ar.fiuba.tdd.tp1.IEvaluable;

import java.util.List;

public class Maximum implements IRangeOperation {

    public Maximum() {

    }

    public final String operate(List<IEvaluable> cellList) {

        double max = cellList.get(0).asDouble();
        for (IEvaluable c : cellList) {
            try {
                Double actual = c.asDouble();
                max = Double.max(max, actual);
            } catch (Exception e) {
                continue;
            }
        }

        return Double.toString(max);
    }
}
