package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.formatters.DoubleFormatter;
import ar.fiuba.tdd.tp1.formatters.FormatterFactory;
import org.joda.time.DateTime;

public class EvaluablesFactory {

    public static IEvaluable getNumber(final double value) {
        return new IEvaluable() {

            private DoubleFormatter formatter = FormatterFactory.getNumberFormatter();

            public double asDouble() {
                return value;
            }

            @Override
            public String getValue() {
                if ((int) value == value) {
                    return String.valueOf((int) value);
                }
                return String.valueOf(value);
            }

            public DoubleFormatter getFormatter() {
                return formatter;
            }
        };
    }

    public static IEvaluable getDate(final String date) {
        return new IEvaluable() {

            private String rawDate = date;
            private double value = (double) (new DateTime(date).getMillis() / 1000.0);
            private DoubleFormatter formatter = FormatterFactory.getDateFormatter();

            public String getValue() {
                return rawDate;
            }

            public double asDouble() {
                return value;
            }

            public DoubleFormatter getFormatter() {
                return formatter;
            }
        };
    }

    public static IEvaluable getMoney(final String symbol, final double wrappingValue) {
        return new IEvaluable() {

            private DoubleFormatter formatter = FormatterFactory.getMoneyFormatter(symbol);

            public String getValue() {
                return formatter.formatDouble(wrappingValue);
            }

            public double asDouble() {
                return wrappingValue;
            }

            public DoubleFormatter getFormatter() {
                return formatter;
            }
        };
    }

    public static IEvaluable getString(String param) {
        return () -> param;
    }
}
