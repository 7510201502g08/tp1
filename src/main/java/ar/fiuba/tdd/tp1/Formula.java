package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.operations.IBinaryOperation;

/**
 * Formula of a Cell. Implements the Composite Pattern, so it's easy to construct and evaluate recursively.
 */
public class Formula implements IEvaluable {

    IEvaluable firstOperand;
    IEvaluable secondOperand;
    IBinaryOperation operator;

    public Formula(IEvaluable firstOperand, IEvaluable secondOperand, IBinaryOperation operator) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
        this.operator = operator;
    }

    public String getValue() {
        return String.valueOf(this.asDouble());
    }

    public double asDouble() {
        double firstOp = firstOperand.asDouble();
        double secondOp = secondOperand.asDouble();
        return operator.operate(firstOp, secondOp);
    }

}
